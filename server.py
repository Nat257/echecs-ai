class Game:

    def __init__(self):
        self.echiquier = {
            '8':{'a':'bR','b':'bN','c':'bB','d':'bQ','e':'bK','f':'bB','g':'bN','h':'bR'},
            '7':{'a':'b','b':'b','c':'b','d':'b','e':'b','f':'b','g':'b','h':'b'},
            '6':{'a':'•','b':'•','c':'•','d':'•','e':'•','f':'•','g':'•','h':'•'},
            '5':{'a':'•','b':'•','c':'•','d':'•','e':'•','f':'•','g':'•','h':'•'},
            '4':{'a':'•','b':'•','c':'•','d':'•','e':'•','f':'•','g':'•','h':'•'},
            '3':{'a':'•','b':'•','c':'•','d':'•','e':'•','f':'•','g':'•','h':'•'},
            '2':{'a':'w','b':'w','c':'w','d':'w','e':'w','f':'w','g':'w','h':'w'},
            '1':{'a':'wR','b':'wN','c':'wB','d':'wQ','e':'wK','f':'wB','g':'wN','h':'wR'},
            }

    def deplacement_accorde(self):
        """Deplace les pieces"""
        echiquier[ligne][colonne] = "•"
        echiquier[ligne2][colonne2] = piece
        self.afficher_plateau()

    def afficher_plateau(self):
        """Affiche le plateau"""
        for i in range(1,9):
            print("{0} | {1} \t{2} \t{3} \t{4} \t{5} \t{6} \t{7} \t{8}".format(9-i,self.echiquier[str(9-i)]["a"],self.echiquier[str(9-i)]["b"],self.echiquier[str(9-i)]["c"],self.echiquier[str(9-i)]["d"],self.echiquier[str(9-i)]["e"],self.echiquier[str(9-i)]["f"],self.echiquier[str(9-i)]["g"],self.echiquier[str(9-i)]["h"]))
        print(" "*4+"-- \t-- \t-- \t-- \t-- \t-- \t-- \t--")
        print(" "*4+"a \tb \tc \td \te \tf \tg \th")

    def possible_moves(self,piece,colonne,ligne):
        """Renvoie les possibilités de mouvement d'une piece"""

        #on trouve le numéro de la colonne
        lettres_colonnes="abcdefgh"
        for i in range(0,8):
            if colonne==lettres_colonnes[i]:
                numero=i

        if piece.startswith('b'):
            couleur = 'b'
        else:
            couleur = 'w'

        sol = []

        #Pion noir
        if piece == 'b':
            if ligne=="7":
                sol.append('b'+colonne+'6')
                sol.append('b'+colonne+'5')
            elif int(ligne) >2:
                sol.append('b'+colonne+str(int(ligne)-1))
            else:
                sol.append('b'+colonne+'1=Q')
                sol.append('b'+colonne+'1=R')
                sol.append('b'+colonne+'1=B')
                sol.append('b'+colonne+'1=N')

            if colonne == 'h':
                if int(ligne) > 2:
                    sol.append('bg'+str(int(ligne)-1))
                else:
                    sol.append('bhxg1=Q')
                    sol.append('bhxg1=R')
                    sol.append('bhxg1=B')
                    sol.append('bhxg1=N')

            elif colonne == 'a':
                if int(ligne) > 2:
                    sol.append('bb'+str(int(ligne)-1))
                else:
                    sol.append('baxb1=Q')
                    sol.append('baxb1=R')
                    sol.append('baxb1=B')
                    sol.append('baxb1=N')

            else:
                if int(ligne) > 2:
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)-1))
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)-1))
                else:
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)-1)+"=Q")
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)-1)+"=Q")

                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)-1)+"=R")
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)-1)+"=R")

                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)-1)+"=B")
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)-1)+"=B")

                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)-1)+"=N")
                    sol.append('b'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)-1)+"=N")

        #Pion blanc
        if piece == 'w':
            if ligne=="2":
                sol.append('w'+colonne+'3')
                sol.append('w'+colonne+'4')
            elif int(ligne) < 7:
                sol.append('w'+colonne+str(int(ligne)+1))
            else:
                sol.append('w'+colonne+'8=Q')
                sol.append('w'+colonne+'8=R')
                sol.append('w'+colonne+'8=B')
                sol.append('w'+colonne+'8=N')

            if colonne == 'h':
                if int(ligne) < 7:
                    sol.append('wg'+str(int(ligne)+1))
                else:
                    sol.append('whxg8=Q')
                    sol.append('whxg8=R')
                    sol.append('whxg8=B')
                    sol.append('whxg8=N')

            elif colonne == 'a':
                if int(ligne) < 7:
                    sol.append('wb'+str(int(ligne)+1))
                else:
                    sol.append('waxb8=Q')
                    sol.append('waxb8=R')
                    sol.append('waxb8=B')
                    sol.append('waxb8=N')

            else:
                if int(ligne) < 7:
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)-1))
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)-1))
                else:
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)+1)+"=Q")
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)+1)+"=Q")

                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)+1)+"=R")
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)+1)+"=R")

                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)+1)+"=B")
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)+1)+"=B")

                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)+1]+str(int(ligne)+1)+"=N")
                    sol.append('w'+colonne+'x'+lettres_colonnes[lettres_colonnes.index(colonne)-1]+str(int(ligne)+1)+"=N")

        #tours blanches / noires
        if piece == "bR" or piece == "wR":
            for l in range(1,9):
                for c in range (1,9):
                    if colonne == lettres_colonnes[c-1] or ligne == str(l):
                        if colonne == lettres_colonnes[c-1] and ligne == str(l):
                            pass
                        else:
                            sol.append(couleur + "R" + lettres_colonnes[c-1] + str(l))
                            sol.append(couleur + "Rx" + lettres_colonnes[c-1] + str(l))


        #cavaliers blancs / noirs
        if piece == "bN" or piece == "wN":
            pass
        #fous blancs / noirs
        if piece == "bB" or piece == "wB":
            liste_l = ['1','2','3','4','5','6','7','8']
            liste_c = ['a','b','c','d','e','f','g','h']
            for a in range(1,9):
                if int(liste_c.index(colonne))+a > 8 or int(liste_l.index(ligne))+a > 8:
                    pass
                else:
                    if liste_c[int(liste_c.index(colonne))+(a-1)]==colonne and liste_l[int(liste_l.index(ligne))+(a-1)]==ligne:
                        pass
                    else:
                        sol.append(couleur + 'B' + liste_c[int(liste_c.index(colonne))+(a-1)] + liste_l[int(liste_l.index(ligne))+(a-1)])
                        sol.append(couleur + 'Bx' + liste_c[int(liste_c.index(colonne))+(a-1)] + liste_l[int(liste_l.index(ligne))+(a-1)])

                if int(liste_c.index(colonne))+a < 1 or int(liste_l.index(ligne))+a < 1:
                    pass
                else:
                    if liste_c[int(liste_c.index(colonne))-(a-1)]==colonne and liste_l[int(liste_l.index(ligne))-(a-1)]==ligne:
                        pass
                    else:
                        sol.append(couleur + 'B' + liste_c[int(liste_c.index(colonne))-(a-1)] + liste_l[int(liste_l.index(ligne))-(a-1)])
                        sol.append(couleur + 'Bx' + liste_c[int(liste_c.index(colonne))-(a-1)] + liste_l[int(liste_l.index(ligne))-(a-1)])

                if int(liste_c.index(colonne))+a > 8 or int(liste_l.index(ligne))+a < 1:
                    pass
                else:
                    if liste_c[int(liste_c.index(colonne))+(a-1)]==colonne and liste_l[int(liste_l.index(ligne))-(a-1)]==ligne:
                        pass
                    else:
                        sol.append(couleur + 'B' + liste_c[int(liste_c.index(colonne))+(a-1)] + liste_l[int(liste_l.index(ligne))-(a-1)])
                        sol.append(couleur + 'Bx' + liste_c[int(liste_c.index(colonne))+(a-1)] + liste_l[int(liste_l.index(ligne))-(a-1)])

                if int(liste_c.index(colonne))+a < 1 or int(liste_l.index(ligne))+a > 8:
                    pass
                else:
                    if liste_c[int(liste_c.index(colonne))-(a-1)]==colonne and liste_l[int(liste_l.index(ligne))+(a-1)]==ligne:
                        pass
                    else:
                        sol.append(couleur + 'B' + liste_c[int(liste_c.index(colonne))-(a-1)] + liste_l[int(liste_l.index(ligne))+(a-1)])
                        sol.append(couleur + 'Bx' + liste_c[int(liste_c.index(colonne))-(a-1)] + liste_l[int(liste_l.index(ligne))+(a-1)])









        #reine blanche / noire
        if piece == "bQ" or piece == "wQ":
            pass
        #roi blanc / noir
        if piece == "bK" or piece == "wK":
            pass


        print(sol)

if __name__ == "__main__":
    game = Game()
    game.afficher_plateau()